'use strict';
const request = require('request');
const config = require('../config.js');

module.exports = {
	fetchEntity : function(input){
		console.log("called 'fetchentity' with input "+ JSON.stringify(input))
		return new Promise((resolve, reject) => {
			var storyId = input.match.substring(1);

			request({
				uri: `https://bat.tpondemand.com/api/v1/UserStories/${storyId}?access_token=${config.access_token}&include=[ID,Name,EntityState]`,
				method: 'GET',
				headers: {'Accept': 'application/json'}
			}, function (err, res, body){
				if(err) return reject(err);
				if(res.statusCode == 200) input.output = JSON.parse(body);

				resolve(input);
			});
		})
	  
	},
    tryFetchBug : function(input){
		console.log("called 'tryFetchBug' with input "+ JSON.stringify(input))
        return new Promise((resolve, reject) => {
        	if(input.output !== null) return resolve(input);

            var bugId = input.match.substring(1);
			var uri = `https://bat.tpondemand.com/api/v1/Bugs/${bugId}?access_token=${config.access_token}&include=[ID,Name,EntityState]`;
            request({
                uri: `https://bat.tpondemand.com/api/v1/Bugs/${bugId}?access_token=${config.access_token}&include=[ID,Name,EntityState]`,
                method: 'GET',
                headers: {'Accept': 'application/json'}
            }, function (err, res, body){
                if(err) return reject(err);
                if(res.statusCode == 200) input.output = JSON.parse(body);
                resolve(input);
            });
        })

    },
	sendResponse : function(inputs){
		console.log("called 'sendResponse' with input "+ JSON.stringify(inputs))
		return new Promise((resolve, reject) => {
			var responseText = {"text":""};

			var response = {
				statusCode: 200,
				body: ''
			};
			inputs.map(function(input){
				if(typeof input.output.Id === "undefined"){

				} else {
					console.log("story name is " + input.output.Name);
					var link = 'https://bat.tpondemand.com/entity/' + input.output.Id;
					responseText["text"] = responseText["text"] + input.output.ResourceType +' <'+link+'|#'+input.output.Id+'>'+': `'+input.output.EntityState.Name+'` '+input.output.Name+'\n'

					console.log(JSON.stringify(responseText))
					response.body = JSON.stringify(responseText)
				}
			});
			resolve(response);
		})
	}
}