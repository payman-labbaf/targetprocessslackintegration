'use strict';
var url = require('url');
const tp = require('./targetprocess.js')


module.exports.fetch = (event, context, callback) => {
  var params = url.parse("www.example.com/?" + event.body, true).query;
  console.log('params are ' + JSON.stringify(params))
  console.log(`${params.user_name} in #${params.channel_name} channel: "${params.text}"`);
  if(params.user_name == 'slackbot'){
    console.log('Ignoring the message since it is from "slackbot"');
    const response = {
        statusCode: 200,
        body: ''
      };
    callback(null, response)
  }
  
  var pattern = /#\d*/g;
    var regexMatch = pattern.exec(params.text);
  var matches = [];
    while(regexMatch != null){
        matches.push(regexMatch[0]);
        regexMatch = pattern.exec(params.text);
    }
  
  if(matches){

      if(!matches.length) callback(null, {});
      var output = null;
      var promises = matches.map(function(match){
        return tp.fetchEntity({match:match, output:output})
        .then(tp.tryFetchBug)

      })

        Promise.all(promises)
            .then(tp.sendResponse)
            .then(response => {
                callback(null, response);
            })
            .catch(err => {
                callback(err);
            })
  }
};